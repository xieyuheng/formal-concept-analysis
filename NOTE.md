# Maybe

- use datomic-like database as formal ctx (table).

  - Temporal concept analysis (TCA)
    It provides animations in concept lattices
    obtained from data about changing objects.
    It offers a general way of understanding
    change of concrete or abstract objects
    in continuous, discrete or hybrid space and time.

# Readings

- implication
  - <https://en.wikipedia.org/wiki/Implication_(information_science)>
  - <https://en.wikipedia.org/wiki/Armstrong%27s_axioms>
- Complete lattice
  - <https://en.wikipedia.org/wiki/Complete_lattice>
- Galois connection
  - <https://en.wikipedia.org/wiki/Galois_connection>
- bicliques & bipartite graph
  - <https://en.wikipedia.org/wiki/Bipartite_graph>
- Knowledge space
  - <https://en.wikipedia.org/wiki/Knowledge_space>
- Mathematical psychology
  - <https://en.wikipedia.org/wiki/Mathematical_psychology>
